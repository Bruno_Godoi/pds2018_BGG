
<?php


if(isset($_POST['name']))
{
    // Include config file
    require_once "config.php";
    //mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_STRICT);
    // Define variables and initialize with empty values
    $titulo = $data = $poster = $sin = $cast = "";
    $param_titulo = $param_data = $param_poster = $param_sin = $param_cast = "";
    $error = "";
    // Processing form data when form is submitted
    if($_SERVER["REQUEST_METHOD"] == "POST"){
    
        // Validate username
        if(empty(trim($_POST["name"]))){
            echo " <script> M.toast({html:'nome vazio :('}) </script> ";
            $error = "true";
        }else{   // Prepare a select statement
            $sql = "SELECT ID FROM filme WHERE Nome = ?";
            
            if($stmt = $mysqli->prepare($sql)){
                // Bind variables to the prepared statement as parameters
                $stmt->bind_param("s", $param_titulo);
                
                // Set parameters
                $param_titulo = trim($_POST["name"]);
                
                // Attempt to execute the prepared statement
                if($stmt->execute()){
                    // store result
                    $stmt->store_result();
                    
                    if($stmt->num_rows == 1){
                        //$username_err = "This email is already taken.";
                        $error = "true";
                        echo " <script> M.toast({html:'Filme já cadastrado!'}) </script> ";                        
                    } else{
                        $titulo = trim($_POST["name"]);
                    }
                } else{
                    $error = "true";
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }
            
            // Close statement
            $stmt->close();
        }
        
        if(empty($error)){
        include_once('php/CheckUpload.php');
        $errorfile = CheckFile();
        }


        if(empty($error) && $errorfile[0] == 1){     
            //echo "ENTRou \n";   
            // Prepare an insert statement
            
            $sql = "INSERT INTO filme ( Nome, Sinopse, Lancamento, Cartaz, Nota, Cast) VALUES (?, ?, ?, ?, ?, ?) ";
            //echo "vai preparar \n";
            if($stmt = $mysqli->prepare($sql)){
                //echo "preparou \n";
                //echo " <script> M.toast({html:'Preparouuu'}) </script> ";
                // Bind variables to the prepared statement as parameters
                $stmt->bind_param("ssssds", $param_titulo,$param_sin, $param_data,$param_poster,$param_nota,$param_cast);
                //echo $titulo;
                // Set parameters
                $param_nota = 0;
                $param_titulo = $titulo;
                $param_sin = $_POST["sin"];
                $param_data = $_POST["release"];
                $param_poster = $errorfile[1];
                $param_cast = $_POST["cast"];
                //echo "vai executar \n";
                // Attempt to execute the prepared statement
                if($stmt->execute()){
                    // Redirect to login page
                    $stmt->close();
                    echo "<script> location.replace('Cadastrar_Filme.php?sucsses=true')</script> ";
                    exit;
                } else{
                    //echo " <script> M.toast({html:'Problema com conexão com o vbanco, tente novamente'}) </script> ";
                }
                $stmt->close();
            }else{
                //echo "ERRO NA PREPARAÇÂO?";
            }
            
            // Close statement
            
        }
        else{
            //echo " <script> M.toast({html:'Deu algum erro antes'}) </script> ";
        }
        
        
        // Close connection
        $mysqli->close();
    }
    //echo " <script> M.toast({html:'cabou mundo'}) </script> ";
}



?>
<!DOCTYPE html>
<html>

    <head>
    <?php include_once('Header.php'); ?>
    </head>

    <body style=" background-color: #525252 ">
        <?php include_once('NavBar.php'); ?>

        <div class="container" style="margin-top: 5%; text-align: center;">
            <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

                <form id="signUp" name="signUp" class="col s12" enctype="multipart/form-data" action="" method="POST" onsubmit="submitt();return false;">
            
                    <div class='row'>
                        <div class='col s12'>
                        </div>
                    </div>

                    <div class='row col s12 '>

                        <div class='row col s4 '>
                            <div class='input-field col s4 pull-s2'>
                            <img  id='image' class="materialboxed" width="200" height ="300" src="https://steemitimages.com/0x0/https://twilightcinema.files.wordpress.com/2017/02/atriptothemoon.jpg">
                            </div>
                        </div>

                        <div class='row col s8 '>

                            <div class='input-field col s12 push-s2'>
                                <input class='validate' type='text' required="required" name ='name' id='name' />
                                <label for='name'>Titulo</label>
                            </div>

                            <div class="formDivider"></div>
                            <div class="formDivider"></div>
                            <div class="formDivider "></div>
                            <div class="formDivider "></div>

                            <div class='input-field col s12  push-s2'>
                                <input class='validate' type='date' required="required" name ='release' id='release' />
                                <label for='release'>Lançamento</label>
                            </div>

                            <div class="formDivider"></div>
                            <div class="formDivider "></div>
                            <div class="formDivider "></div>
                            <div class="formDivider"></div>
            
        

                            <div class="file-field input-field col s12 push-s2">

                                <div class="btn">
                                    <span>Cartaz</span>
                                    <input type="file" id='files' name="files"> 
                                </div>

                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" id="files2" required="required">
                                    <label for='files2'>Faça Upload do cartaz</label>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class='row'>
                        <div class='input-field col s12'>
                            <textarea class='materialize-textarea' type='text' maxlenght="250" required="required" name='sin' id='sin'></textarea>
                            <label for='sin'>Sinopse</label>
                        </div>
                
                        <div class='input-field col s12'>
                            <textarea class='materialize-textarea' type='text' maxlenght="250" required="required" name='cast' id='cast'></textarea>
                            <label for='cast'>Elenco - Separar nomes por ;</label>
                        </div>
            
                    
                
<script>
    function submitt(e){
                document.getElementById("signUp").submit();
    };
</script>                
                        
                    <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect padrao' >Cadastrar</button>
                

                    
            </div>
        </div>

    <br />
   
    </body>

    <?php include_once('Footer.php'); ?>


</html>

<?php
if(isset($_GET['sucsses'])){
      if($_GET["sucsses"] == true){
        echo " <script> M.toast({html:'Filme adicionado com sucesso!'}) </script> ";
        $url = strtok($url, '?');
    }
}
?>

<script>
document.getElementById("files").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("image").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};

   $(document).ready(function(){
    $('.materialboxed').materialbox();
  });
</script>
