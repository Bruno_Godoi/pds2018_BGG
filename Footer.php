</main>
<footer class="page-footer padrao">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">

            <p class="grey-text text-lighten-4">Netoflix é uma marca registrada</p>
          </div>
          <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Links</h5>
            <ul>
              <li><a class="grey-text text-lighten-3" href="#!">facebook.com/Netoflix</a></li>
              <li><a class="grey-text text-lighten-3" href="#!">Twitter.com/Netoflix</a></li>

            </ul>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
        © 2014 Copyright Text
        <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
      </div>
    </footer>

  <!--JavaScript at end of body for optimized loading-->
  <script type="text/javascript" src="js/materialize.min.js"></script>