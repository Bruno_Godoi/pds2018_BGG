<!DOCTYPE html>
<html>
  <?php include_once('Header.php'); ?>

  <body style=" background-color: #525252 ">
  <?php include_once('NavBar.php'); ?>
  <div class="container">
  

    <script>
      $(document).ready(function() {
        var instance = M.Carousel.init({
          fullWidth: true,
          indicators: true
        });

        $('.carousel.carousel-slider').carousel({
          fullWidth: true,
          indicators: true
        });
      });
    </script>

    <div class="carousel carousel-slider center small">
        <a class="carousel-item" href="MoviePage.php?MovieId=14">
          <img src="https://www.joblo.com/assets/images/oldsite/posters/images/full/inception-uk-banner2.jpg">
        </a>
        <a class="carousel-item" href="MoviePage.php?MovieId=9">
          <img src="https://www.dravenstales.ch/wp-content/uploads/2015/05/mad-max-fury-road-banner.jpg">
        </a>
        <a class="carousel-item" href="MoviePage.php?MovieId=8">
          <img src="http://anygoodfilms.com/wp-content/uploads/2018/10/bohemian-rhapsody-teaser-poster.jpg">
        </a>
        <a class="carousel-item" href="MoviePage.php?MovieId=1">
          <img src="https://i2.wp.com/splashreport.com/wp-content/uploads/2018/07/aquaman-quad.png?resize=851%2C315">
        </a>

    </div>

    <h3 class="header " style="height: 27px; ">Novos no Netoflix</h3>
    <hr/>

    <div class="row" >
      <div class="col s12 m12 l12 ">
        <?php 
          include_once('Card.php'); 
          GetLastCards();
        ?>
      </div>
    </div>

    <?php include_once('Footer.php'); ?>
    </div>
  </body>

</html>

<script>
  $('input[name="rating"]').change(function(){
    var stars = this.value;
    console.log(stars);
  })
</script>