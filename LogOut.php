<?php 
// After User Click On Logout page.
 require 'config.php'; // Connection
 if(!isset($_SESSION)) session_start();    // Session start
?>


 <?php
    

    if(isset($_SESSION['loggedin'])){

     unset($_SESSION["loggedin"]);
     session_destroy();
     session_unset();    
     header('Location: Index.php');

      }
    
 ?>