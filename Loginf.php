
<!DOCTYPE html>
<html>

<head>
<?php include_once('Header.php'); ?>
</head>

<body style=" background-color: #525252" >

<?php include_once('NavBar.php'); ?>



  <div class="container" style="margin-top: 5%; text-align: center;">
    <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

      <form class="col s12" action="" method="POST" onsubmit="Login(); return false;">
        <div class='row'>
          <div class='col s12'>
          </div>
        </div>

        <div class='row'>
          <div class='input-field col s12'>
            <input class='validate' type='email' required="required" name ='email' id='email' />
            <label for='email'>Enter your email</label>
          </div>
        </div>

        <div class='row'>
          <div class='input-field col s12'>
            <input class='validate' type='password' minlength="6" required="required" name='password' id='password' />
            <label for='password'>Enter your password</label>
          </div>
          <label style='float: right;'>
            <a class='pink-text' href='#!'><b>Forgot Password?</b></a>
          </label>
        </div>

        <br />

        <div class='row'>
          <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect padrao'>Login</button>
        </div>

      </form>
    </div>
  </div>
  <div style="text-align: center">
    <a href="SignUpf.php">Create account</a>

  </div>
  <div class="section"></div>
  <div class="section"></div>
  </main>

<?php include_once('Footer.php');?>
</body></html>
<?php 
   if(isset($_POST['email']))
   {
      Login();
   }

   if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    echo "<script> location.replace('Index.php')</script> ";
    exit;
  }
if(isset($_GET["conta"])){
  if($_GET["conta"] == true){
  echo "<script> M.toast({html:'Conta criada com sucesso, por favor faça login'}) </script>";
  $url = strtok($url, '?');
}
}
?>
<?php
function Login(){
  // Initialize the session
  //echo " <script> M.toast({html:'Teste 2 vai vai'}) </script> ";
  if(!isset($_SESSION)) session_start();
  
  // Check if the user is already logged in, if yes then redirect him to welcome page
  
  
  // Include config file
  require_once "config.php";
  
  // Define variables and initialize with empty values
  $username = $password = "";
  $username_err = $password_err = "";

  // Processing form data when form is submitted
  if($_SERVER["REQUEST_METHOD"] == "POST"){
      //echo ("<Div> Teve Post <br></Div>");
      // Check if username is empty
      if(empty(trim($_POST["email"]))){
          $username_err = "Please enter username.";
      } else{
          $username = trim($_POST["email"]);
      }
      
      // Check if password is empty
      if(empty(trim($_POST["password"]))){
          $password_err = "Please enter your password.";
      } else{
          $password = trim($_POST["password"]);
      }
      
      // Validate credentials
      if(empty($username_err) && empty($password_err)){
          // Prepare a select statement
          $sql = "SELECT id, Nome, Email, Senha FROM user WHERE Email = ?";
          
          if($stmt = $mysqli->prepare($sql)){
              // Bind variables to the prepared statement as parameters
              $stmt->bind_param("s", $param_username);
              
              // Set parameters
              $param_username = $username;
              
              // Attempt to execute the prepared statement
              if($stmt->execute()){
                  // Store result
                  $stmt->store_result();
                  
                  // Check if username exists, if yes then verify password
                  if($stmt->num_rows == 1){                    
                      // Bind result variables
                      echo " <script> M.toast({html:'Encontrou na base'}) </script> ";
                      $stmt->bind_result($id, $nome, $username, $hashed_password);
                      if($stmt->fetch()){
                          //echo ("<Div> password <br></Div>". $password);
                          if(password_verify($password, $hashed_password)){
                              // Password is correct, so start a new session
                              if(!isset($_SESSION)) session_start();
                              
                              // Store data in session variables
                              $_SESSION["loggedin"] = true;
                              $_SESSION["id"] = $id;
                              $_SESSION["username"] = $nome;
                              $_SESSION["mail"] = $username;                            
                              
                              // Redirect user to welcome page
                              echo "<script> location.replace('Index.php')</script> ";
                          } else{
                              // Display an error message if password is not valid
                              $password_err = "The password you entered was not valid.";
                              echo " <script> M.toast({html:'Usuario ou senha incoretos :(0'}) </script> ";
                          }
                      }
                  } else{
                      // Display an error message if username doesn't exist
                      //$username_err = "No account found with that username.";
                      echo " <script> M.toast({html:'Usuario ou senha incoretos :(1'}) </script> ";
                  }
              } else{
                  echo "Oops! Something went wrong. Please try again later.";
              }
          }
          
          // Close statement
          $stmt->close();
      }
      
      // Close connection
      $mysqli->close();
  }
}
?>