
<?php
 
if(isset($_POST['email']))
{
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate username
    if(empty(trim($_POST["email"]))){
        echo " <script> M.toast({html:'Email vazio :('}) </script> ";
    } else{
        // Prepare a select statement
        $sql = "SELECT ID FROM user WHERE Email = ?";
        
        if($stmt = $mysqli->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bind_param("s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["email"]);
            
            // Attempt to execute the prepared statement
            if($stmt->execute()){
                // store result
                $stmt->store_result();
                
                if($stmt->num_rows == 1){
                    //$username_err = "This email is already taken.";
                    echo " <script> M.toast({html:'Email já cadastrado :('}) </script> ";
                } else{
                    $email = trim($_POST["email"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        $stmt->close();
    }
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        echo " <script> M.toast({html:'senha vazia :('}) </script> ";   
    } elseif(strlen(trim($_POST["password"])) < 6){
        echo " <script> M.toast({html:'Senha tem que ter pelo menos 6 caracteres :('}) </script> ";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
    
    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        
        // Prepare an insert statement
        $sql = "INSERT INTO user (Nome,Email , Senha) VALUES (?, ?, ?)";
         
        if($stmt = $mysqli->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bind_param("sss", $param_username,$param_Email , $param_password);
            
            // Set parameters
            $param_username = trim($_POST["name"]);
            $param_Email = $email;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            
            // Attempt to execute the prepared statement
            if($stmt->execute()){
                // Redirect to login page
                header("location: Loginf.php?conta=true");

            } else{
                echo "Deu problema no banco";
            }
        }
         
        // Close statement
        $stmt->close();
    }
    
    // Close connection
    $mysqli->close();
}
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('Header.php'); ?>
</head>

<body style=" background-color: #525252 ">
<?php include_once('NavBar.php'); ?>

  <div class="container" style="margin-top: 5%; text-align: center;">
    <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

      <form id="signUp" name="signUp" class="col s12" action="" method="POST" onsubmit="submitt(); return false;">
      
        <div class='row'>
          <div class='col s12'>
          </div>
        </div>

        <div class='row'>
            <div class='input-field col s6 '>
              <input class='validate' type='text' required="required" name ='name' id='name' />
              <label for='name'>Como deseja ser chamado?</label>
            </div>
          

        
          <div class='input-field col s6 '>
            <input class='validate' type='email' required="required" name ='email' id='email' />
            <label for='email'>Coloque seu email</label>
            </div>
        </div>

        <div class='row'>
          <div class='input-field col s6'>
            <input class='validate' type='password' minlength="6" required="required" name='password' id='password' />
            <label for='password'>Coloque sua senha</label>
          </div>
          
       

        
            <div class='input-field col s6'>
              <input required="required" name='confirm_password' id="passwordConfirm" type="password">
              <label id="lblPasswordConfirm" for="passwordConfirm" >Confirme sua senha</label>
              <span  id="pcspan" class="helper-text" data-error="Password not match" data-success="Password Match"></span>
            </div>
            
        </div>

        <br />

<script>
function submitt(e){

if ($("#password").val() !== $("#passwordConfirm").val()) {
    M.toast({html:'Senhas estão diferentes :('})
    document.getElementById("passwordConfirm").focus();
   
} else{
        document.getElementById("signUp").submit();
       
}

};
</script>

        <div class='row'>
          <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect padrao'  >Cadastrar</button>
        </div>

      </form>
    </div>
  </div>
  <div class="section"></div>
  <div class="section"></div>
  

</div>
</div>


<?php include_once('Footer.php'); ?>

</body>



</html>
<script>

  $("#password").on("focusout", function (e) {
    if ($("#password").val() !== $("#passwordConfirm").val()) {
        $("#pcspan").removeClass("valid").addClass("invalid");
        $("#passwordConfirm").removeClass("valid").addClass("invalid");
      
    } else {
        $("#pcspan").removeClass("invalid").addClass("valid");
        $("#passwordConfirm").removeClass("invalid").addClass("valid");
      
    }
});

$("#passwordConfirm").on("keyup", function (e) {
    if ($("#password").val() !== $(this).val()) {
        $("#pcspan").removeClass("valid").addClass("invalid");
        $("#passwordConfirm").removeClass("valid").addClass("invalid");
       
    } else {
        $("#pcspan").removeClass("invalid").addClass("valid");
        $("#passwordConfirm").removeClass("invalid").addClass("valid");
        
    }
});
</script>

