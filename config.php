<?php
/* Database credentials. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'netoflix');
/* Attempt to connect to MySQL database */
$mysqli = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
//mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_STRICT);
// Check connection
if($mysqli === false){
   die("ERROR: Could not connect. " . $mysqli->connect_error);
}
if(!isset($_SESSION)) session_start();
    
         
     

//mysqli_report(MYSQLI_REPORT_ALL ^ MYSQLI_REPORT_STRICT);
class Database extends MySQLi {
    private static $instance = null ;

    private function __construct($host, $user, $password, $database){
        parent::__construct($host, $user, $password, $database);
    }

    public static function getInstance(){
        if (self::$instance == null){
            self::$instance = new self("localhost", 'root', '', 'netoflix');
        }
        return self::$instance ;
    }

    public static function reset(){
       self::$instance = null;
   }
}
?>
