-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29-Nov-2018 às 20:57
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `netoflix`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

CREATE TABLE `comentarios` (
  `ID` int(11) NOT NULL,
  `IDFilme` int(11) NOT NULL,
  `IDUser` int(11) NOT NULL,
  `Titulo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Comentario` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Nota` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `comentarios`
--

INSERT INTO `comentarios` (`ID`, `IDFilme`, `IDUser`, `Titulo`, `Comentario`, `Nota`) VALUES
(71, 5, 1, 'MUITO BOOOOM', '', 4.5),
(72, 5, 2, '', '', 5),
(73, 5, 3, '', '', 4.5),
(74, 5, 1, '', '', 5),
(75, 6, 2, '', '', 5),
(76, 6, 3, '', '', 1),
(77, 6, 1, '', '', 3),
(78, 6, 2, '', '', 4),
(80, 7, 2, 'Ansioso!', '', 3.5),
(81, 16, 3, 'Interessante', 'A mente criminosa e destrutiva dos indivÃ­duos psicopatas em O silÃªncio dos inocentes denota a ira descomunal destas criaturas incomensuravelmente perversas.', 5),
(82, 16, 1, 'Incrivel!!!', 'Adoro todos os Filmes de Hannibal, na minha opiniÃ£o ele Ã© vilÃ£o mais bem construÃ­do do Cinema Hannibal Ã© Inteligente, Frio, Educado, Macabro, SanguinÃ¡rio, Paciente, calmo, Manipulador, Concentrado no quer e etc', 4.5),
(107, 9, 2, 'bom!', 'Muito bom!! Oscars merecidos!!', 4),
(108, 9, 3, 'Maravilhoso', 'O filme Ã© incrÃ­vel, desde o enredo atÃ© a atuaÃ§Ã£o. Tudo impecÃ¡vel!', 5),
(109, 15, 1, 'Surreal', 'Simplesmente surreal. InigualÃ¡vel.', 5),
(110, 15, 2, '3', 'Tive que assistir umas 3 vezes para entender, por isso dei nota 3.', 3),
(111, 7, 3, 'Vai ser muito bom', 'Filme com certeza serÃ¡ incrÃ­vel! Mais um dos clÃ¡ssicos da Disney regravado que vai ganhar todos os prÃªmios.', 3.5),
(112, 14, 1, 'D+', '', 5),
(113, 14, 2, 'Fraco', 'Achei fraco. Estava esperando bem mais desse filme.', 1.5),
(114, 8, 3, 'Excelente', 'Filme Ã³timo para assistir em famÃ­lia, muito nostÃ¡lgico', 5),
(115, 8, 1, 'Meio parado', '', 2),
(116, 10, 2, 'RecomendadÃ­ssimo!', 'Marcante', 5),
(117, 10, 3, 'Triste', '', 2),
(118, 11, 1, 'Classico', 'NÃ£o importa quantas vezes, sempre que eu assisto ache ele incrivel e me surpreendo', 5),
(119, 11, 2, 'NÃ£o tem igual', '', 5),
(120, 12, 3, 'NÃ£o entendi', '', 0.5),
(121, 12, 1, 'Complexo e profundo', '', 5),
(122, 12, 2, 'Muito bom', 'Quem fica falando que nÃ£o entendeu Ã© porque nÃ£o tem a capacidade de conceber quÃ£o grandiosa Ã© essa obra.', 5),
(124, 13, 3, '', '', 5),
(125, 13, 1, '', '', 0.5),
(126, 13, 2, 'Filme Ã©pico ...', '', 5),
(127, 13, 3, 'Eu amo esse filme', '', 5),
(128, 13, 3, 'Eu amo esse filme', '', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `filme`
--

CREATE TABLE `filme` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(100) NOT NULL,
  `Sinopse` text NOT NULL,
  `Lancamento` date NOT NULL,
  `Cartaz` varchar(100) NOT NULL,
  `Nota` float NOT NULL,
  `Cast` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `filme`
--

INSERT INTO `filme` (`ID`, `Nome`, `Sinopse`, `Lancamento`, `Cartaz`, `Nota`, `Cast`) VALUES
(1, 'Aquaman', 'Metade humano e semi atlante, Arthur Curry (Jason Momoa) parte em uma jornada junto de seus aliados Mera (Amber Heard) e Vulko (Willem Dafoe) para encontrar o tridente do rei, uma arma mÃ­tica capaz de controlar os sete mares. O trio precisa cumprir seu objetivo antes de Orm (Patrick Wilson), meio-irmÃ£o de Arthur que pretende derrubÃ¡-lo e tomar o trono de AtlÃ¢ntida.', '2018-12-13', 'Posters/Aquaman.jpg', 0, 'Jason Momoa como Arthur Curry / Aquaman: O rei da naÃ§Ã£o submarina de AtlÃ¢ntida, com a capacidade de manipular as marÃ©s do oceano, se comunicar com outras vidas aquÃ¡ticas e nadar em velocidades supersÃ´nicas. Otis Dhanji interpreta o jovem Arthur Curry.[2]\r\nAmber Heard como Mera: A rainha da naÃ§Ã£o submarina da AtlÃ¢ntida e interesse amoroso de Arthur. Mera possui poderes hidrocinÃ©ticos e telepÃ¡ticos que lhe permitem controlar seu ambiente aquÃ¡tico e se comunicar com outros Atlantes.[3]\r\nYahya Abdul-Mateen II como Arraia Negra: Um implacÃ¡vel caÃ§ador de tesouros e mercenÃ¡rio. Ele Ã© o arqui-inimigo do Aquaman.\r\nWillem Dafoe como Nuidis Vulko: Conselheiro cientÃ­fico principal de AtlÃ¢ntida.\r\nDolph Lundgren como Nereus: O rei da naÃ§Ã£o submarina de Xebel, terra natal de Mera.[4]\r\nPatrick Wilson como Orm Marius / Mestre do Oceano: O meio-irmÃ£o corrompido de Arthur, que procura tomar o trono de AtlÃ¢ntida. Wilson tambÃ©m teve um papel cameo em Batman v Superman: Dawn of Justice, fornecendo a voz do Presidente dos Estados Unidos.\r\nRandall Park como Dr. Stephen Shin: Um cientista especializado em estudo submarino,ajudou Arthur Curry a controlar seus poderes atlantes, porÃ©m Arthur e seu pai Thomas se afastaram de Shin por conta de sua obsessÃ£o em descobrir a localizaÃ§Ã£o de AtlÃ¢ntida.\r\nDjimon Hounsou como Rei Fisherman: Um dos marqueses dos sete mares de Atlantis e aliado de Orm contra Aquaman. Hounsou tambÃ©m interpreta o Mago Shazam em Shazam!.\r\nTemuera Morrison como Thomas Curry: Um faroleiro e pai de Aquaman.\r\nNicole Kidman como Atlanna: A mÃ£e de Arthur Curry e ex-rainha de AtlÃ¢ntida.[5]\r\nLudi Lin como Murk: O lÃ­der dos Homens-de-Guerra, o exÃ©rcito de linha de frente de AtlÃ¢ntida.'),
(2, 'Ghost Busters', 'Erin Gilbert Ã© uma professora na Universidade de Columbia que espera se tornar efetivada. Um dia, Ã© contratada pelo dono de uma mansÃ£o que diz ser mal-assombrada baseado em um livro sobre vida apÃ³s a morte que Erin co-escreveu, mas havia imaginado nÃ£o existir mais. Indo atrÃ¡s da co-autora, sua ex-amiga Abby Yates, que continua pesquisando o sobrenatural em outra faculdade com a ajuda da excÃªntrica Jillian Holtzmann, Erin convence Abby a tirar o livro de circulaÃ§Ã£o, com a condiÃ§Ã£o de seguir ela e Holtzmann para a mansÃ£o. Ao investigar, encontram um fantasma, que vomita em Erin antes de fugir do lugar. As trÃªs ficam felizes em provarem a existÃªncia do sobrenatural, mas o registro da visita acaba por causar a demissÃ£o de Erin. Sem opÃ§Ãµes, acaba tendo que se unir a Abby e Holtzmann, que tambÃ©m sÃ£o despejadas de sua instituiÃ§Ã£o, para criar um grupo para pesquisar e capturar fantasmas. Montando escritÃ³rio em cima de um restaurante chinÃªs e contratando o belo mas pouco inteligente Kevin como seu recepcionista, acabam por encontrar um fantasma nos tÃºneis do metrÃ´ apÃ³s serem levadas lÃ¡ por uma funcionÃ¡ria do lugar, Patty Tolan, que acaba se juntando ao grupo por seu conhecimento histÃ³rico de Nova Iorque e a promessa de conseguir um veÃ­culo, que acaba por ser um carro fÃºnebre. Depois de serem contratadas para capturar um fantasma que assombrava um teatro, as CaÃ§a-Fantasmas percebem que a atividade sobrenatural estÃ¡ elevada, com grandes riscos para toda Nova York.', '2016-07-14', 'Posters/Ghost Busters.jpg', 3.3, 'Melissa McCarthy como Abby Yates[3]\r\nKristen Wiig como Erin Gilbert[3]\r\nKate McKinnon como Jillian Holtzmann[3]\r\nLeslie Jones como Patty Tolan[3]\r\nChris Hemsworth como Kevin Beckman[4]\r\nCecily Strong como Jennifer Lynch[5]\r\nAndy GarcÃ­a como Prefeito Bradley[6]\r\nNeil Casey como Rowan North[7]\r\nCharles Dance como Harold Filmore\r\nMichael K. Williams como Agente Hawkins[6]\r\nMatt Walsh como Agente Rourke[6]'),
(7, 'The Lion King', 'The Lion King Ã© um futuro filme Ã©pico musical de drama estadunidense de 2019, dirigido e co-produzido por Jon Favreau e escrito por Jeff Nathanson, sendo o remake do longa-metragem animado O Rei LeÃ£o, de 1994, inspirado em partes da obra Hamlet, de William Shakespeare', '2019-07-19', 'Posters/The Lion King.jpg', 3.5, 'Donald Glover as Simba, the crown prince of the Pride Lands.\r\nJD McCrary as young Simba;BeyoncÃ© Knowles-Carter as Nala, Simba\'s childhood best friend and future love-interest.;Shahadi Wright Joseph as young Nala\r\nJames Earl Jones as Mufasa, the king of the Pride Lands and the father of Simba. Jones reprises his role from the original 1994 animated film.;Chiwetel Ejiofor as Scar, the treacherous brother of Mufasa and the uncle of Simba who seeks to take the mantle of king.;Billy Eichner as Timon, a wise-cracking and loyal meerkat who befriends and adopts a young Simba after he runs away from home.;Seth Rogen as Pumbaa, a naive common warthog who befriends and adopts a young Simba after he runs away from home.'),
(8, 'Bohemian Rhapsody', 'Bohemian Rhapsody is a 2018 biographical film about the British rock band Queen. It follows singer Freddie Mercury\'s life, leading to Queen\'s Live Aid performance at Wembley Stadium in 1985. The film is a British-American joint venture produced by 20th Century Fox, New Regency, GK Films, and Queen Films, with Fox serving as distributor.', '2018-10-23', 'Posters/Bohemian Rhapsody.png', 3.5, 'Rami Malek as Freddie Mercury / Farrokh Bulsara, lead vocalist of the rock band Queen;Lucy Boynton as Mary Austin, Mercury\'s girlfriend;Gwilym Lee as Brian May, Queen\'s lead guitarist;Ben Hardy as Roger Taylor, Queen\'s drummer;Joe Mazzello as John Deacon, Queen\'s bass guitarist;Aidan Gillen as John Reid, Queen\'s manager;Allen Leech as Paul Prenter, Mercury\'s personal manager;Tom Hollander as Jim Beach, Queen\'s lawyer turned manager;Mike Myers as Ray Foster, an EMI executive'),
(9, 'Mad Max Estrada da Furia', 'Mad Max: Estrada da FÃºria(em inglÃªs: Mad Max: Fury Road) Ã© um filme australiano-estadunidense de 2015, dos gÃªneros aÃ§Ã£o e ficÃ§Ã£o cientÃ­fica, dirigido por George Miller e escrito por Miller, Brendan McCarthy e Nico Lathouris. Ã‰ o quarto tÃ­tulo da franquia Mad Max, se passando em um vasto deserto de um futuro pÃ³s-apocalÃ­ptico onde gasolina e Ã¡gua sÃ£o bens valiosos. Ele segue a histÃ³ria de Max Rockatansky, que se junta a Imperatriz Furiosa para fugir do lÃ­der cultista Immortan Joe e seu exÃ©rcito dentro de um caminhÃ£o tanque. O filme Ã© estrelado por Tom Hardy, Charlize Theron, Nicholas Hoult, Hugh Keays-Byrne, Rosie Huntington-Whiteley, Riley Keough, ZoÃ« Kravitz, Abbey Lee e Courtney Eaton.', '2015-05-14', 'Posters/Mad Max Estrada da Furia.jpg', 4.5, 'Tom Hardy como Max RockatanskyCharlize Theron como Imperatriz FuriosaNicholas Hoult como NuxHugh Keays-Byrne como Immortan JoeRosie Huntington-Whiteley como EsplÃªndida AngharadRiley Keough como CapableZoÃ« Kravitz como ToastAbbey Lee como DagCourtney Eaton como CheedoJosh Helman como Slit'),
(10, 'Batman O Cavaleiro das Trevas', 'Na cidade de Gotham, o Coringa e seus comparsas roubam um banco pertencente a mÃ¡fia. Batman e o Tenente James Gordon decidem incluir o novo promotor pÃºblico Harvey Dent, que estÃ¡ namorando Rachel Dawes, no plano para acabar com a mÃ¡fia. Bruce mais tarde encontra Dent e promete fazer uma festa para arrecadar fundos para ele, depois de perceber sua sinceridade. Os chefes da mÃ¡fia Sal Maroni, Gambol e Chechen se encontram com outros chefes criminais para discutir os novos problemas nas suas atividades. Lau, um contador chinÃªs, conta que ele escondeu todo o dinheiro e fugiu para Hong Kong para impedir que o plano de Gordon se concretize. O Coringa invade a reuniÃ£o avisando que o Batman vai caÃ§ar Lau, e se oferece para matÃ¡-lo em troca de metade do dinheiro. Todos recusam e Gambol pÃµe um preÃ§o pela cabeÃ§a do Coringa. Pouco tempo depois o Coringa mata Gambol e recruta seus homens.', '2008-06-18', 'Posters/Batman O Cavaleiro das Trevas.jpg', 3.5, 'Christian Bale;Michael Caine;Heath Ledger;Gary Oldman;Aaron Eckhart;Maggie Gyllenhaal\r\nMorgan Freeman;'),
(11, 'O Senhor dos Aneis O Retorno do Rei', 'Sauron planeja um grande ataque a Minas Tirith, capital de Gondor, o que faz com que Gandalf (Ian McKellen) e Pippin (Billy Boyd) partam para o local na intenÃ§Ã£o de ajudar a resistÃªncia. Um exÃ©rcito Ã© reunido por Theoden (Bernard Hill) em Rohan, em mais uma tentativa de deter as forÃ§as de Sauron. Enquanto isso Frodo (Elijah Wood), Sam (Sean Astin) e Gollum (Andy Serkins) seguem sua viagem rumo Ã  Montanha da PerdiÃ§Ã£o, para destruir o Um Anel.', '2003-12-25', 'Posters/O Senhor dos Aneis O Retorno do Rei.jpg', 5, 'Elijah Wood;Ian McKellen;Viggo Mortensen;Liv Tyler;Sean Astin;Cate Blanchett;John Rhys-;avies\r\nBernard Hill;Billy Boyd;Dominic Monaghan;Orlando Bloom;Hugo Weaving;Miranda Otto;David Wenham;Karl Urban;John Noble;Andy Serkis;Ian Holm'),
(12, 'Donnie Darko', 'Donnie Darko is a 2001 American science fiction film written and directed by Richard Kelly. It stars Jake Gyllenhaal, Jena Malone, Drew Barrymore, Mary McDonnell, Katharine Ross, Patrick Swayze, Noah Wyle, and Maggie Gyllenhaal. The film follows the adventures of the troubled title character as he seeks the meaning behind his doomsday-related visions.', '2001-10-26', 'Posters/Donnie Darko.jpg', 3.5, 'Jake Gyllenhaal as Donald \"Donnie\" Darko;Holmes Osborne as Eddie Darko;Maggie Gyllenhaal as Elizabeth Darko;Daveigh Chase as Samantha Darko;Mary McDonnell as Rose Darko;James Duval as Frank Anderson;Arthur Taxier as Dr. Fisher;Patrick Swayze as Jim Cunningham;David St. James as Bob Garland;Jazzie Mahannah as Joanie James;Jolene Purdy as Cherita Chen;Stuart Stone as Ronald Fisher;Gary Lundy as Sean Smith;Alex Greenwald as Seth Devlin;Beth Grant as Kitty Farmer;Jena Malone as Gretchen Ross;Seth Rogen as Ricky Danforth'),
(13, 'Psycho', 'Psycho is a 1960 American psychological horror film directed and produced by Alfred Hitchcock, and written by Joseph Stefano. It stars Anthony Perkins, Janet Leigh, John Gavin, Vera Miles, and Martin Balsam, and was based on the 1959 novel of the same name by Robert Bloch. The film centers on an encounter between a secretary, Marion Crane (Leigh), who ends up at a secluded motel after stealing money from her employer, and the motel\'s owner-manager, Norman Bates (Perkins), and its aftermath.[5]', '1960-09-08', 'Posters/Psycho.jpg', 4.1, 'Anthony Perkins as Norman Bates;Janet Leigh as Marion Crane;Vera Miles as Lila Crane;John Gavin as Sam Loomis;Martin Balsam as Private Investigator Milton Arbogast;John McIntire as ;eputy Sheriff Al Chambers;Simon Oakland as Dr. Fred Richman;Frank Albertson as Tom ;assidy\r\nPat Hitchcock as Caroline;Vaughn Taylor as George Lowery;Lurene Tuttle as Mrs. Chambers;John Anderson as California Charlie;Mort Mills as Highway Patrol Officer;Francis De Sales as Deputy District Attorney Alan Deats (uncredited);George Eldredge as Police Chief ;ames Mitchell (uncredited);Ted Knight as Police Guard (uncredited);Virginia Gregg, Paul Jasmin, and Jeanette Nolan as the voice of Norma \"Mother\" Bates (uncredited). The three voices were used interchangeably, except for the last speech, which was performed by Gregg'),
(14, 'A Origem', 'Em um mundo onde Ã© possÃ­vel entrar na mente humana, Cobb (Leonardo DiCaprio) estÃ¡ entre os melhores na arte de roubar segredos valiosos do inconsciente, durante o estado de sono. AlÃ©m disto ele Ã© um fugitivo, pois estÃ¡ impedido de retornar aos Estados Unidos devido Ã  morte de Mal (Marion Cotillard). Desesperado para rever seus filhos, Cobb aceita a ousada missÃ£o proposta por Saito (Ken Watanabe), um empresÃ¡rio japonÃªs: entrar na mente de Richard Fischer (Cillian Murphy), o herdeiro de um impÃ©rio econÃ´mico, e plantar a ideia de desmembrÃ¡-lo. Para realizar este feito ele conta com a ajuda do parceiro Arthur (Joseph Gordon-Levitt), a inexperiente arquiteta de sonhos Ariadne (Ellen Page) e Eames (Tom Hardy), que consegue se disfarÃ§ar de forma precisa no mundo dos sonhos.', '2010-08-06', 'Posters/A Origem.jpg', 3.3, 'Leonardo DiCaprio;Ken Watanabe;Joseph Gordon-Levitt;Marion Cotillard;Ellen Page;Tom Hardy;Cillian Murphy;Dileep Rao;Tom Berenger;Michael Caine'),
(15, 'MATRIX', 'Em um futuro prÃ³ximo, Thomas Anderson (Keanu Reeves), um jovem programador de computador que mora em um cubÃ­culo escuro, Ã© atormentado por estranhos pesadelos nos quais encontra-se conectado por cabos e contra sua vontade, em um imenso sistema de computadores do futuro. Em todas essas ocasiÃµes, acorda gritando no exato momento em que os eletrodos estÃ£o para penetrar em seu cÃ©rebro. Ã€ medida que o sonho se repete, Anderson comeÃ§a a ter dÃºvidas sobre a realidade. Por meio do encontro com os misteriosos Morpheus (Laurence Fishburne) e Trinity (Carrie-Anne Moss), Thomas descobre que Ã©, assim como outras pessoas, vÃ­tima do Matrix, um sistema inteligente e artificial que manipula a mente das pessoas, criando a ilusÃ£o de um mundo real enquanto usa os cÃ©rebros e corpos dos indivÃ­duos para produzir energia. Morpheus, entretanto, estÃ¡ convencido de que Thomas Ã© Neo, o aguardado messias capaz de enfrentar o Matrix e conduzir as pessoas de volta Ã  realidade e Ã  liberdade.', '1999-05-21', 'Posters/MATRIX.jpg', 4, 'Keanu Reeves;Laurence Fishburne;Carrie-Anne Moss;Hugo Weaving;Joe Pantoliano;O SilÃªncio dos Inocentes'),
(16, 'O SilÃªncio dos Inocentes', 'A agente do FBI, Clarice Starling (Jodie Foster) Ã© ordenada a encontrar um assassino que arranca a pele de suas vÃ­timas. Para entender como ele pensa, ela procura o periogoso psicopata, Hannibal Lecter (Anthony Hopkins), encarcerado sob a acusaÃ§Ã£o de canibalismo.', '1991-05-17', 'Posters/O SilÃªncio dos Inocentes.jpg', 4.5, 'odie Foster como Clarice StarlingMasha Skorobogatov como jovem Clarice Starling;Anthony ;opkins como Dr. Hannibal Lecter;;cott Glenn como Jack CrawfordTed Levine como Jame ;;Buffalo Bill\" GumpAnthony Heald como Dr. Frederick Chilton;Brooke Smith como Catherine ;;artinKasi Lemmons como Ardelia Mapp;;Frankie Faison como Barney Matthews;Diane Baker omo Senadora Ruth Martin;;Tracey Walter como Lamar;Charles Napier como Tenente Boyle;Danny Darst como Sargento Tate;Ales Coleman como Sargento Jim Pembry;Dan Butler ;omo Roden;Paul Lazar como Pilcher;Ron Vawter como Paul Krendler;Roger Corman como iretor do FBI Hayden Burke;George A. Romero Agente do FBI lotado em Menphis (nÃ£o ;reditado);Chris Isaak como Comandante da SWAT;Harry Northup como Sr. Bimmel;');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(150) NOT NULL,
  `Email` varchar(150) NOT NULL,
  `Senha` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`ID`, `Nome`, `Email`, `Senha`) VALUES
(1, 'Leon700', 'BG@xxx.com', '$2y$10$Ma011bbKDZx.ldadhb0yk.9n80QQeQw/i7uOGAsGbN7KbJnf4Dx6.'),
(2, 'Ricardo Cinefilo', 'R@E.I', '$2y$10$/UVBbotWzrHdRJ8y9VcDjO8ox1FnevzfRmi.IaB2EEzyygGw.dkG.'),
(3, 'Amante de filmes', 'BG@x.com', '$2y$10$NGbDfMRDs/ZNy4IKk0o8f.5ptSBUWKTmF2bQsdcI.zgPvRTqWsfZu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `filme`
--
ALTER TABLE `filme`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `filme`
--
ALTER TABLE `filme`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
