<?php
function CheckFile() {
    $target_dir = "Posters/";
    $target_file = $target_dir . basename($_FILES["files"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    $path_parts = pathinfo($_FILES["files"]["name"]);
    $extension = $path_parts['extension'];
    $destination = "Posters/".$_POST["name"].".". $extension;

    // Check if image file is a actual image or fake image
    if(isset($_POST["name"])) {
        $check = getimagesize($_FILES["files"]["tmp_name"]);
        if($check !== false) {
            //echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            //echo "File is not an image.";
            $uploadOk = 0;
        }
    }
   
    // Check file size
    if ($_FILES["files"]["size"] > 5000000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["files"]["tmp_name"], $destination)) {
           // echo "The file ". basename( $_FILES["files"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }

     return array($uploadOk, $destination);;
}
?>