<?php



function SearchAndGetMovie($id)
{
    require_once "config.php";
    $db = Database::getInstance();
    //$result = $db->query("SELECT 1 FROM table");
          
    $sql = "SELECT Nome,Sinopse,Lancamento,Cartaz,Nota,Cast FROM filme WHERE ID = ?";


            
    if($stmt = $db->prepare($sql)){
              // Bind variables to the prepared statement as parameters
        $stmt->bind_param("d", $param_id);
              
              // Set parameters
        $param_id = $id;
              
              // Attempt to execute the prepared statement
        if($stmt->execute()){
                //echo "executou";
                  // store result
            $stmt->bind_result($name, $sin,$Lan,$poster,$nota,$cast);

            while ($stmt->fetch()) {
                   // echo $name;
                   GetMovie($name,$Lan,$sin,$poster,$nota,$cast);
            }
            } else{
                $error = "true";
                echo "Oops! Something went wrong. Please try again later.";
            }

            $db->close();
    }
}


function GetMovie($name,$Lan,$sin,$poster,$nota,$cast)
{
echo"  <div class='container' style='margin-top: 5%; text-align: center;'>
            <div class='z-depth-1 grey lighten-4 row col l10' style='display: inline-block; padding: 32px 48px -60px 48px; border: 1px solid #EEE;'>
      
                <div class='input-field col s12 m4 l4'>
                    <img  id='image' class='materialboxed responsive-img'  src='$poster'>
                </div>
                <div class='col s8 m8 l8 '>
                    <h4 style='text-align: left;'>$name</h4> 
                    <h6 style=' position: relative ;top: -10px;  text-align: left;'>$Lan</h6>
                    <h6 style=' position: relative ;top: -10px;  text-align: left;'>Nota: $nota/5</h6>
                </div>

                <div class='input-field col l7 m7'>
                    <h5 style='position: relative ;text-align: left;top: -30px;'>Elenco:</h5>
                    <h6 style='position: relative ;text-align: left;top: -30px;'>$cast</h6>
                </div>


                <div class='input-field col l8 m7'>
                    <h5 style='position: relative ;text-align: left;top: -70px;'>Sinopse:</h5>
                    <h6 style='position: relative ;text-align: left;top: -70px;'>$sin</h6>
                </div>
                
            </div>
        </div>";

}

?>
